import sys
import scipy.fftpack
import numpy as np
from numpy.linalg import norm
import argparse
import matplotlib.pyplot as plt

from ax.utils import read_audio
from ax.spectrum import compute_spectrum, prepare_mel_filters
from ax.onset import compute_spectral_flux

parser = argparse.ArgumentParser()
parser.add_argument('file', help='audio file')
args = parser.parse_args()

fs = 44100 # sampling frequency
nfft = 1024 # 23 ms
overlap = 512 # 11 ms

min_freq = 26
max_freq = 16000

sig = read_audio(args.file, fs, 25)
x, t = compute_spectrum(sig, nfft, nfft, overlap)

filters = prepare_mel_filters(64, min_freq, max_freq, nfft, fs, norm=True)[0]

mel = np.dot(x, filters)

plt.subplot(2, 1, 1)
plt.title('Spectrum')
extent = float(t[0]) / fs, float(t[-1]) / fs, 0, fs / 2
plt.imshow(np.log(np.flipud(x.T) + 0.0000000001), extent=extent, interpolation='nearest', aspect='auto')
#plt.imshow(np.flipud(x.T), extent=extent, interpolation='nearest', aspect='auto')

sf = compute_spectral_flux(mel)

f = np.hamming(7)
csf = np.convolve(np.concatenate((sf[3:], np.zeros(3))), f / norm(f))[:-6]

f = np.ones(31)
D_t = np.convolve(np.concatenate((csf[15:], np.zeros(15))), f / norm(f, 1))[:-30]

plt.subplot(2, 1, 2)
plt.title('Spectral flux')
plt.xlim(0, sf.shape[0])
#plt.plot(sf)
plt.plot(csf)
plt.plot(D_t)

csf = csf - D_t
csf[csf<0] = 0

d_csf = csf - np.concatenate((np.zeros(1), csf[:-1]))

#plt.subplot(3, 1, 3)
#plt.title('peaks')
#plt.xlim(0, sf.shape[0])
#plt.plot(d_csf)

peaks = []
for i in range(1, d_csf.shape[0]):
    if d_csf[i-1] > 0 > d_csf[i] and d_csf[i-1] - d_csf[i] > 0.005:
        peaks.append(i - 1)
        plt.axvline(i - 1, c='black')

print peaks


plt.show()

