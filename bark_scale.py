import numpy as np
import matplotlib.pyplot as plt

from ax.spectrum import freq_to_bark

f = np.linspace(0, 11025, 1000)

plt.plot(f, freq_to_bark(f))
plt.xlabel('freq')
plt.ylabel('bark')
plt.grid('on')
plt.show()

