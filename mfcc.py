import sys
import scipy.fftpack
import numpy as np
import matplotlib.pyplot as plt

from ax.utils import read_audio
from ax.spectrum import compute_spectrum, prepare_mel_filters


f0 = 440 # frequency of the test signal
f1 = 3000 # frequency of the test signal
fs = 8000 # sampling frequency

sig = read_audio(sys.argv[1], fs, 3)
x, t = compute_spectrum(sig, 512, 256, 256 - 256 / 8)

filters = prepare_mel_filters(36, 60, 3600, 512, fs)[0]

mel = np.dot(x, filters)

plt.subplot(311)
plt.title('Spectrum')
extent = float(t[0]) / fs, float(t[-1]) / fs, 0, fs / 2
plt.imshow(np.flipud(x.T), extent=extent, interpolation='nearest', aspect='auto')

plt.subplot(312)
plt.title('Mel')
extent = float(t[0]) / fs, float(t[-1]) / fs, 0, mel.shape[1]
plt.imshow(np.flipud(mel.T), extent=extent, interpolation='nearest', aspect='auto')

mfcc = scipy.fftpack.dct(mel)

plt.subplot(313)
plt.title('MFCC')
extent = float(t[0]) / fs, float(t[-1]) / fs, 0, mfcc.shape[1]
plt.imshow(np.flipud(mfcc.T), extent=extent, interpolation='nearest', aspect='auto')

#plt.subplot(313)
#summed_mel = np.sum(mel, axis=1)
#plt.plot(summed_mel)
#plt.xlim(0, summed_mel.shape[0])
#plt.xticks([])

plt.show()

