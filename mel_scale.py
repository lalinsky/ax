import numpy as np
import matplotlib.pyplot as plt

from ax.spectrum import freq_to_mel

f = np.linspace(0, 11025, 1000)

plt.plot(f, freq_to_mel(f))
plt.xlabel('freq')
plt.ylabel('mel')
plt.grid('on')
plt.show()

