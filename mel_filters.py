import argparse
import numpy as np
import matplotlib.pyplot as plt

from ax.spectrum import prepare_mel_filters

parser = argparse.ArgumentParser()
parser.add_argument('-b', type=int, dest='num_bands', metavar='BANDS', default=12, help='number of bands')
parser.add_argument('-n', action='store_true', dest='normalize', default=False, help='normalize')
args = parser.parse_args()

num_bands = args.num_bands
filters, peaks = prepare_mel_filters(num_bands, 130, 5400, 256, 11025, args.normalize)

plt.subplot(2, 1, 1)
for i in range(1, num_bands + 1):
    if args.normalize:
        height = 2.0 / (peaks[i+1] - peaks[i-1])
    else:
        height = 1.0
    plt.plot([peaks[i-1], peaks[i], peaks[i+1]], [0, height, 0])
    plt.hold(True)

plt.subplot(2, 1, 2)
plt.imshow(np.flipud(filters.T), interpolation='nearest')

plt.show()

