import numpy as np


def compute_spectral_flux(x):
#    x = np.log(5 * x + 1)
    sf = np.empty(x.shape)
    sf[0,] = np.zeros(x.shape[1])
    for i in xrange(1, x.shape[0]):
        sf[i,] = np.abs(x[i,]) - np.abs(x[i-1,])
    sf[sf<0] = 0
    return np.sum(sf, axis=1)

