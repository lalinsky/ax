import struct
import audioread
import numpy as np
from scikits.samplerate import resample


def read_audio(filename, fs, max_length=None):
    data = []
    with audioread.audio_open(filename) as file:
        if max_length is not None:
            max_samples = max_length * file.samplerate
        else:
            max_samples = None
        for buffer in file:
            item_size = struct.calcsize('<h')
            num_items = len(buffer) / item_size
            data.extend(struct.unpack('<%dh' % num_items, buffer))
            if max_samples is not None and len(data) > max_samples:
                data = data[:max_samples]
                break
        signal = np.array(data, dtype=np.float) / 32768.0
        if file.channels == 2:
            signal = signal[0::2] + signal[1::2]
        elif file.channels != 1:
            raise ValueError('multi-channel audio is not supported')
        return resample(signal, float(fs) / float(file.samplerate), 'sinc_medium')

