import sys
import scipy.fftpack
import numpy as np


def compute_spectrum(x, n=256, window=None, overlap=None):
    """Calculates a spectrogram for the input audio data.

    Args:
        x: Audio data vector.
        n: FFT frame size. Default: 256.
        window: Audio window size. Default: the same as the FFT frame size.
        overlap: Overlap with the previous window. Default: window/2.

    Returns:
        A 2-element tuple containing:
            - 2D matrix with the magnitude spectrum, one row per FFT frame.
            - Vector with the sample offset for each FFT frame.
    """

    if window is None:
        window = n
    if overlap is None:
        overlap = window / 2

    step = window - overlap
    offsets = np.arange(0, x.shape[0] - window, step)
    frames = np.array([x[o:o+window] for o in offsets]) * np.hanning(window)

    if window < n:
        padding = np.zeros((frames.shape[0], n - window))
        frames = np.hstack((frames, padding))

    return np.abs(np.fft.rfft(frames)) / n, offsets


def freq_to_mel(f):
    """Converts frequency to mel.

    http://en.wikipedia.org/wiki/Mel_scale

    Args:
        f: Frequency in Hz.

    Returns:
        Mel value.
    """

    return 2595 * np.log10(1 + f / 700.)


def mel_to_freq(m):
    """Converts mel to frequency.

    http://en.wikipedia.org/wiki/Mel_scale

    Args:
        m: Mel value.

    Returns:
        Frequency in Hz.
    """

    return 700 * (np.power(10, m / 2595.) - 1)


def freq_to_bark(f):
    """Converts frequency to bark.

    http://en.wikipedia.org/wiki/Bark_scale

    Args:
        f: Frequency in Hz.

    Returns:
        Bark value.
    """

    return 13 * np.arctan(0.76 * f / 1000.) + 3.5 * np.arctan(f / 7500.)


def prepare_mel_filters(num_bands, freq_min, freq_max, n, fs, norm=True):
    mel_min = freq_to_mel(freq_min)
    mel_max = freq_to_mel(freq_max)
    mel_bw = (mel_max - mel_min) / (num_bands + 1)

    peaks = np.array([mel_to_freq(mel_min + mel_bw * i) for i in
                      range(0, num_bands + 2)])

    delta_freq = fs * 1.0 / n

    filters = np.zeros((num_bands, n / 2 + 1))
    for i in range(1, num_bands + 1):
        left, center, right = peaks[i-1:i+2]
        height = 2.0 / (right - left)
        bin = np.ceil(left / delta_freq)
        freq = bin * delta_freq
        while freq < right:
            if freq <= center:
                value = (freq - left) / (center - left)
            else:
                value = 1 - (freq - center) / (right - center)
            filters[i-1,bin] = value
            freq += delta_freq
            bin += 1

    if norm:
        filters = filters.transpose() / np.sum(filters, axis=1)
    else:
        filters = filters.transpose()

    return filters, peaks


def prepare_bark_filters(num_bands, min_freq, max_freq, n, fs, norm=True):
    min_bark = freq_to_bark(min_freq)
    max_bark = freq_to_bark(max_freq)
    bw = (min_bark - max_bark) / num_bands

    peaks = np.array([mel_to_freq(min_bark + max_bark * i) for i in
                      range(num_bands)])

    delta_freq = fs * 1.0 / n

    filters = np.zeros((num_bands, n / 2 + 1))
    for i in range(1, num_bands + 1):
        left, center, right = peaks[i-1:i+2]
        height = 2.0 / (right - left)
        bin = np.ceil(left / delta_freq)
        freq = bin * delta_freq
        while freq < right:
            if freq <= center:
                value = (freq - left) / (center - left)
            else:
                value = 1 - (freq - center) / (right - center)
            filters[i-1,bin] = value
            freq += delta_freq
            bin += 1

    if norm:
        filters = filters.transpose() / np.sum(filters, axis=1)
    else:
        filters = filters.transpose()

    return filters, peaks

